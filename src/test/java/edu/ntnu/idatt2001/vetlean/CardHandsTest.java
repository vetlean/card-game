package edu.ntnu.idatt2001.vetlean;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CardHandsTest {

    private PokerCardHand testDataFlush(){
        ArrayList<PlayingCard> sameSuit = new ArrayList<>();
        sameSuit.add( new PlayingCard('H',1));
        sameSuit.add( new PlayingCard('H',2));
        sameSuit.add( new PlayingCard('H',3));
        sameSuit.add( new PlayingCard('H',4));
        sameSuit.add( new PlayingCard('H',5));

        PokerCardHand flush = new PokerCardHand(sameSuit);
        return flush;
    }
    private PokerCardHand testDataFlushWithEightCards(){
        ArrayList<PlayingCard> sameSuit = new ArrayList<>();
        sameSuit.add( new PlayingCard('S',1));
        sameSuit.add( new PlayingCard('H',2));
        sameSuit.add( new PlayingCard('H',3));
        sameSuit.add( new PlayingCard('H',4));
        sameSuit.add( new PlayingCard('H',5));
        sameSuit.add( new PlayingCard('H',6));
        sameSuit.add( new PlayingCard('H',7));
        sameSuit.add( new PlayingCard('H',8));

        PokerCardHand flush = new PokerCardHand(sameSuit);
        return flush;
    }

    private PokerCardHand testDataHighCard(){
        ArrayList<PlayingCard> hand = new ArrayList<>();
        hand.add( new PlayingCard('H',1));
        hand.add( new PlayingCard('C',5));
        hand.add( new PlayingCard('D',3));
        hand.add( new PlayingCard('H',10));
        hand.add( new PlayingCard('S',8));

        PokerCardHand highCard = new PokerCardHand(hand);
        return highCard;
    }

    private PokerCardHand testDataStraight(){
        ArrayList<PlayingCard> card = new ArrayList<>();
        card.add( new PlayingCard('H',1));
        card.add( new PlayingCard('C',2));
        card.add( new PlayingCard('D',3));
        card.add( new PlayingCard('H',4));
        card.add( new PlayingCard('S',5));

        PokerCardHand straight = new PokerCardHand(card);
        return straight;
    }

    private PokerCardHand testDataQueenOfSpades(){
        ArrayList<PlayingCard> card = new ArrayList<>();
        card.add( new PlayingCard('H',1));
        card.add( new PlayingCard('C',6));
        card.add( new PlayingCard('D',8));
        card.add( new PlayingCard('H',5));
        card.add( new PlayingCard('S',12));

        PokerCardHand straight = new PokerCardHand(card);
        return straight;
    }

    @Test
    public void TestIsFlush(){
        PokerCardHand flush = testDataFlush();
        PokerCardHand notflush = testDataHighCard();
        PokerCardHand flushEightCards = testDataFlushWithEightCards();

        assertTrue(flush.isFlush());
        assertFalse(notflush.isFlush());
        assertTrue(flushEightCards.isFlush());
    }

    @Test
    public void testIsStraight(){
        PokerCardHand straight = testDataStraight();
        PokerCardHand  highCard = testDataHighCard();

        assertTrue(straight.isStraight());
        assertFalse(highCard.isStraight());
    }

    @Test
    public void testTotalValue(){
        PokerCardHand  highCard = testDataHighCard();
        PokerCardHand straight = testDataStraight();

        assertEquals(27, highCard.totalValue());
        assertEquals(15,straight.totalValue());

    }

    @Test
    public void testFilterForHarts(){
        PokerCardHand flush = testDataFlush();
        PokerCardHand highCard = testDataHighCard();
        PokerCardHand straight = testDataStraight();

        assertEquals("H1 H2 H3 H4 H5 ",flush.getAllHarts());
        assertEquals("H1 H10 ",highCard.getAllHarts());
        assertEquals("H1 H4 ",straight.getAllHarts());

    }

    @Test
    public void testContaonsQueenOfSpades(){
        PokerCardHand hasQeenOfSpades = testDataQueenOfSpades();
        PokerCardHand noQeenOfSpades = testDataHighCard();

        assertTrue(hasQeenOfSpades.containsQueenOfSpades());
        assertFalse(noQeenOfSpades.containsQueenOfSpades());
    }

    @Test
    public void testRefillDeck(){
        DeckOfCards baseDeck = new DeckOfCards();
        DeckOfCards testDeck = new DeckOfCards();
        testDeck.removeCardFromDeck(new PlayingCard('S',1));
        testDeck.removeCardFromDeck(new PlayingCard('S',2));
        testDeck.removeCardFromDeck(new PlayingCard('S',3));
        testDeck.removeCardFromDeck(new PlayingCard('S',4));
        testDeck.removeCardFromDeck(new PlayingCard('S',5));

        testDeck.refillDeck();
        assertTrue(testDeck.getCards().equals(baseDeck.getCards()));
    }
}
