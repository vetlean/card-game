package edu.ntnu.idatt2001.vetlean;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardTest {

    @Test
    public void testCreationOfDeck(){
        DeckOfCards deck = new DeckOfCards();
        assertNotNull(deck);
        assertEquals(52, deck.getCards().size());
    }

    @Test
    public void testRemoveCardFromDeck(){
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52, deck.getCards().size());
        deck.removeCardFromDeck(new PlayingCard('S',1));
        assertEquals(51,deck.getCards().size());
        deck.removeCardFromDeck(new PlayingCard('S',2));
        assertEquals(50,deck.getCards().size());
        deck.removeCardFromDeck(new PlayingCard('S',3));
        assertEquals(49,deck.getCards().size());
    }

    @Test
    public void testDealHandNumberOfCards(){
        DeckOfCards deck = new DeckOfCards();

        PokerCardHand handOneCard = new PokerCardHand(deck.dealHand(1));
        assertEquals(1,handOneCard.getHand().size());
        PokerCardHand handTwoCards = new PokerCardHand(deck.dealHand(2));
        assertEquals(2,handTwoCards.getHand().size());
        PokerCardHand handThreeCards = new PokerCardHand(deck.dealHand(3));
        assertEquals(3,handThreeCards.getHand().size());
        PokerCardHand handFourCards = new PokerCardHand(deck.dealHand(4));
        assertEquals(4,handFourCards.getHand().size());
        PokerCardHand handElevenCards = new PokerCardHand(deck.dealHand(11));
        assertEquals(11,handElevenCards.getHand().size());
    }


}
