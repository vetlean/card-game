package edu.ntnu.idatt2001.vetlean;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import static javafx.application.Application.launch;

public class Client extends Application{

    /**
     * The main method that starts the window
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }


    /**
     * the start method for javaFX to show window
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage){
        try {
            System.out.println(getClass().getResource("card-game.fxml"));
            Parent root = FXMLLoader.load(Client.class.getResource("card-game.fxml"));

            primaryStage.setTitle("Card game");

            Scene scene = new Scene(root,800,600);

            primaryStage.setScene(scene);

            primaryStage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
