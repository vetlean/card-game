package edu.ntnu.idatt2001.vetlean;

import java.util.*;
import java.util.stream.Collectors;

public class PokerCardHand {

    List<PlayingCard> hand;

    public PokerCardHand(List<PlayingCard> hand){
        this.hand = hand;
    }

    /**
     * test if the hand has a flush. 5 card og the same suit.
     * @return a boolean. true if it has a flush
     */
    public boolean isFlush(){
        return hand.stream().collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()))
                .values().stream().anyMatch(x -> x >= 5);

    }

    /**
     * test if the hand has a straight. 5 cards increasing.
     * @return a boolean. true if it has a straight
     */
    public boolean isStraight(){
         hand.sort(Comparator.comparingInt(PlayingCard::getFace));
         for (int i = 0; i<hand.size()-1;i++){
             if(hand.get(i+1).getFace() - hand.get(i).getFace() != 1){
                 return false;
             }
         }
         return true;
    }

    /**
     * finds the total value og the hand
     * @return an int, the total value
     */
    public int totalValue(){
        int sum = hand.stream().mapToInt(PlayingCard::getFace).sum();
        return sum;
    }

    /**
     * finds all the harts og the hand
     * @return string if all the harts
     */
    public String getAllHarts(){
        StringBuilder stringBuilder = new StringBuilder();

        hand.stream().filter(card -> card.getSuit() == 'H').collect(Collectors.toList())
                .forEach(playingCard -> stringBuilder.append(playingCard.getAsString()+" "));

        return stringBuilder.toString();
    }

    /**
     * makes a string of the hand
     * @return a string of the hand
     */
    public String getStringRepresentationHand(){

        return this.hand.stream()
                .map(playingCard -> Character.toString(playingCard.getSuit()) + playingCard.getFace())
                .collect(Collectors.joining(" "));
    }

    /**
     * finds out if the hand has a queen
     * @return a boolean, true if it has a queen
     */
    public boolean containsQueenOfSpades(){
        return hand.stream().anyMatch(playingCard -> playingCard.getFace() == 12 && playingCard.getSuit()=='S');
    }

    /**
     * @return the hand
     */
    public List<PlayingCard> getHand() {
        return hand;
    }
}
