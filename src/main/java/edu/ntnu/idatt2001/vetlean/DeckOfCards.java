package edu.ntnu.idatt2001.vetlean;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {

    /**
     * the constructor of the deck. makes 52 different cards.
     */
    private final char[] suit = {'S','H','D','C'};
    private ArrayList<PlayingCard> cards;
    private Random random;

    public DeckOfCards(){
        this.cards = new ArrayList<>();
        this.random = new Random();
        for (char c : suit) {
            for(int i = 1; i<14; i++) {
                this.cards.add(new PlayingCard(c, i));
            }
        }
    }

    /**
     * removes a card from the deck.
     * @param card PlayingCard
     */
    public void removeCardFromDeck(PlayingCard card){
        cards.remove(card);
    }

    /**
     * deals the hand in the game
     * @param n the amount of cards in the game
     * @return a hand of cards
     */
    public List<PlayingCard> dealHand(int n){
       List<PlayingCard> hand = new ArrayList<>();
       PlayingCard cardDealt = null;
        for(int i = 0; i<n;i++){
            cardDealt = this.cards.get(random.nextInt(cards.size()));
            hand.add(cardDealt);
            removeCardFromDeck(cardDealt);
        }
        return hand;
    }

    /**
     * refills the deck after use
     * @return a  refilled dek of cards
     */
    public List<PlayingCard> refillDeck(){
        cards.removeAll(cards);
        for (char c : suit) {
            for(int i = 1; i<14; i++) {
                this.cards.add(new PlayingCard(c, i));
            }
        }
        return cards;
    }

    public ArrayList<PlayingCard> getCards() {
        return cards;
    }
}
