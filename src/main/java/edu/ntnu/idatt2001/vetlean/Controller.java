package edu.ntnu.idatt2001.vetlean;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    private PokerCardHand hand;
    private DeckOfCards deck;

    @FXML
    public TextField sumOfFaces;
    @FXML
    public TextField isFlush;
    @FXML
    public TextField cardOfHearts;
    @FXML
    public TextField queenOfSpades;
    @FXML
    public TextField straight;
    @FXML
    public Label labelDealCards;


    /**
     * The constructor for controller. initializes the deck.
     */
    public Controller(){
        deck = new DeckOfCards();
    }

    /**
     * Deals the card and shows it one the window.
     * @param event
     */
    public void dealCards(ActionEvent event){
        hand = new PokerCardHand(deck.dealHand(5));
        String handAsString = hand.getStringRepresentationHand();
        labelDealCards.setText(handAsString);
        deck.refillDeck();
    }

    /**
     * Checks the hand for different values.
     * @param event the click on the button.
     */
    public void checkCards(ActionEvent event){

        try {
            if(hand.getAllHarts().isBlank()){
                cardOfHearts.setText("No Hearts");
            }else {
                cardOfHearts.setText(hand.getAllHarts());
            }
            isFlush.setText(String.valueOf(hand.isFlush()));
            straight.setText(String.valueOf(hand.isStraight()));
            sumOfFaces.setText(String.valueOf(hand.totalValue()));

            if (hand.containsQueenOfSpades()){
                queenOfSpades.setText("yes");
            }else{
                queenOfSpades.setText("no");
            }
        }catch (NullPointerException e){
            e.getMessage();
        }
    }


}
